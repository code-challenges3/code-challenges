package com.joshparker.challenges.find_closest_two_points;

/**
 * Worst Case Time Complexity: O(n2)
 * Best Case Time Complexity: O(n)
 * Average Time Complexity: O(n2)
 */
public final class SolutionTWO {

    private SolutionTWO() {}

    public static int[][] getClosestTwoPoints(int[][] points) {
        int[][] sortedPoints = bubbleSortPointDifferences(points);
        return new int[][]{sortedPoints[0], sortedPoints[1]};
    }

    private static int[][] bubbleSortPointDifferences(int[][] points) {
        for (int i = 0; i < points.length - 1; i++) {
            boolean hasSwapped = false;
            for (int j = 0; j < points.length - 1; j++) {
                int currDiff = getDiffBetweenPoint(points[j]);
                int nextDiff = getDiffBetweenPoint(points[j + 1]);
                if (currDiff > nextDiff) {
                    hasSwapped = true;
                    int[] holding = points[j];
                    points[j] = points[j + 1];
                    points[j + 1] = holding;
                }
            }
            if (!hasSwapped) {
                return points;
            }
        }
        return points;
    }

    private static int getDiffBetweenPoint(int[] point) {
        return point[0] <= point[1] ? point[1] - point[0] : point[0] - point[1];
    }
}
