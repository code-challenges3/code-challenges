package com.joshparker.challenges.find_closest_two_points;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionONETest {

    @Test
    void getClosestTwoPoints_givenSetOfPoints_returnsTwoClosestPoints() {
        int[][] points = new int[][]{{1, 1}, {-1, -1}, {3, 4}, {6, 1}, {-1, -6}, {-4, -3}};

        int[][] result = SolutionONE.getClosestTwoPoints(points);

        int[][] expectedResult = new int[][]{{1,1},{-1,-1}};
        assertArrayEquals(expectedResult, result);
    }
}