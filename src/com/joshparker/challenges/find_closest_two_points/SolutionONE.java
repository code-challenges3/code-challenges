package com.joshparker.challenges.find_closest_two_points;

/**
 * Time complexity O(n)
 */
public final class SolutionONE {

    private SolutionONE() {}

    public static int[][] getClosestTwoPoints(int[][] points) {
        int lowestDiffIndexOne = 0;
        int lowestDiffOne = getDiffBetweenPoint(points[0]);
        int lowestDiffIndexTwo = 1;
        int lowestDiffTwo = getDiffBetweenPoint(points[1]);

        for (int i = 2; i < points.length; i++) {
            int currDiff = getDiffBetweenPoint(points[i]);
            if (currDiff < lowestDiffOne && currDiff < lowestDiffTwo) {
                lowestDiffIndexOne = i;
                lowestDiffOne = currDiff;
            } else if (currDiff < lowestDiffOne && currDiff > lowestDiffTwo) {
                lowestDiffIndexOne = i;
                lowestDiffOne = currDiff;
            } else if (currDiff < lowestDiffTwo && currDiff > lowestDiffOne) {
                lowestDiffIndexTwo = i;
                lowestDiffTwo = currDiff;
            }
        }
        return new int[][]{points[lowestDiffIndexOne], points[lowestDiffIndexTwo]};
    }

    private static int getDiffBetweenPoint(int[] point) {
        return point[0] <= point[1] ? point[1] - point[0] : point[0] - point[1];
    }
}
