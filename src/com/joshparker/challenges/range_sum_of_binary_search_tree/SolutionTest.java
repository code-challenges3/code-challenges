package com.joshparker.challenges.range_sum_of_binary_search_tree;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given a binary search tree and a range [a, b] (inclusive),
 * return the sum of the elements of the binary search tree within the range.
 *
 * For example, given the following tree:
 *
 *     5
 *    / \
 *   3   8
 *  / \ / \
 * 2  4 6  10
 *
 * and the range [4, 9], return 23 (5 + 4 + 6 + 8).
 */
class SolutionTest {

    @Test
    void getRangeSumOfBST_givenBinarySearchTree_returnsCorrectRangeSum() {
        BinarySearchTree bst = generateBinarySearchTree();
        int result = Solution.getRangeSumOfBST(bst, 4, 9);
        Assertions.assertEquals(result, 23);
    }

    private BinarySearchTree generateBinarySearchTree() {
        BinarySearchTree bst = new BinarySearchTree();
        bst.insert(5);
        bst.insert(3);
        bst.insert(2);
        bst.insert(4);
        bst.insert(8);
        bst.insert(6);
        bst.insert(10);
        return bst;
    }
}
