package com.joshparker.challenges.range_sum_of_binary_search_tree;

class Node {
    int value;
    Node leftNode;
    Node rightNode;

    public Node(int val) {
        this.value = val;
    }
}
