package com.joshparker.challenges.range_sum_of_binary_search_tree;

class BinarySearchTree {

   Node rootNode;

    void insert(int val) {
        rootNode = insertNode(rootNode, val);
    }

    private Node insertNode(Node node, int val) {
        if (node == null) {
            node = new Node(val);
            return node;
        } else if (val < node.value) {
            node.leftNode = insertNode(node.leftNode, val);
        } else if (val > node.value) {
            node.rightNode = insertNode(node.rightNode, val);
        }
        return node;
    }
}