package com.joshparker.challenges.range_sum_of_binary_search_tree;

import java.util.Stack;

/**
 * Given a binary search tree and a range [a, b] (inclusive),
 * return the sum of the elements of the binary search tree within the range.
 *
 * For example, given the following tree:
 *
 *     5
 *    / \
 *   3   8
 *  / \ / \
 * 2  4 6  10
 *
 * and the range [4, 9], return 23 (5 + 4 + 6 + 8).
 */
class Solution {

    private Solution() {}

    static int getRangeSumOfBST(BinarySearchTree tree, int startRange, int endRange) {
        int sum = 0;
        Stack<Node> stack = new Stack<>();
        stack.push(tree.rootNode);
        while (!stack.isEmpty()) {
            Node node = stack.pop();
            if (node != null) {
                if (startRange <= node.value && node.value <= endRange)
                    sum += node.value;
                if (startRange < node.value)
                    stack.push(node.leftNode);
                if (node.value < endRange)
                    stack.push(node.rightNode);
            }
        }
        return sum;
    }
}
